package com.cit.plugins

import com.cit.common.CallbackCodeResponse
import com.cit.common.Prefixes.Companion.API
import com.cit.controllers.UserController
import com.cit.respondAnswer
import com.cit.respondCodeResponse
import com.cit.respondExceptionError
import com.cit.respondNullExceptionErrorQueryParameters
import io.ktor.server.application.*
import io.ktor.server.routing.*

fun Application.configureUsersRouting() {

    val userController = UserController()

    routing {
        get(API.profile){
            try {
                val id = call.request.queryParameters["id"]!!.toInt()
                val user = userController.getUser(id)
                if (user != null)
                    call.respondAnswer(user.toSafe())
                else
                    call.respondCodeResponse(CallbackCodeResponse.USER_NOT_FOUND)
            }catch(e: NullPointerException){
                call.respondNullExceptionErrorQueryParameters(e, listOf("id"))
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }

        get(API.me){
            try {
                val user = userController.getUserFromCallToken(call)?.toSafe() ?: return@get
                call.respondAnswer(user)
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }
    }
}