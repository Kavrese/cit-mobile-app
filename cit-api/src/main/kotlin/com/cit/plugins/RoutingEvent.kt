package com.cit.plugins

import com.cit.*
import com.cit.common.*
import com.cit.common.Prefixes.Companion.API
import com.cit.common.models.ModelTime
import com.cit.controllers.EventController
import com.cit.controllers.UserController
import io.ktor.server.routing.*
import io.ktor.server.application.*

fun Application.configureEventsRouting() {

    val eventController = EventController()
    val userController = UserController()

    routing {
        get(API.events){
            try {
                val user = userController.getUserFromCallToken(call) ?: return@get
                val events = eventController.getUserEvents(user.id)
                call.respondAnswer(events)
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }

        get(API.eventsDay){
            try {
                val date = call.request.queryParameters["date"]!!
                val user = userController.getUserFromCallToken(call) ?: return@get
                if (date.validateDate(datePattern)){
                    val events = eventController.getUserEventsDay(user.id, date.toPatternDate(datePattern))
                    val allTimeEvents = events.map{it.time}.toSet().toList()
                    val modelTimes = allTimeEvents.map{ time ->
                        ModelTime(time, events.filter { it.time == time })
                    }
                    call.respondAnswer(modelTimes)
                }else{
                    call.respondCodeResponse(CallbackCodeResponse.DATE_INCORRECT_DATE)
                }
            }catch (e: NullPointerException){
                call.respondNullExceptionErrorQueryParameters(e, listOf("date"))
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }

        get(API.daysWithEventData){
            try {
                val start = call.request.queryParameters["start"]!!
                val end = call.request.queryParameters["end"] ?: start
                val user = userController.getUserFromCallToken(call) ?: return@get
                if (start.validateDate(datePattern) && end.validateDate(datePattern)){
                    val dateStart = start.toPatternDate(datePattern)
                    val dateEnd = end.toPatternDate(datePattern)
                    val days = eventController.getDaysWithEventsUser(user.id, dateStart, dateEnd)
                    call.respondAnswer(eventController.getUserEventsDays(days, user.id))
                }else{
                    call.respondCodeResponse(CallbackCodeResponse.DATE_INCORRECT_DATE)
                }
            }catch (e: NullPointerException){
                call.respondNullExceptionErrorQueryParameters(e, listOf("start"))
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }

        get(API.daysWithEvent){
            try {
                val start = call.request.queryParameters["start"]!!
                val end = call.request.queryParameters["end"] ?: start
                val user = userController.getUserFromCallToken(call) ?: return@get
                if (start.validateDate(datePattern) && end.validateDate(datePattern)){
                    val dateStart = start.toPatternDate(datePattern)
                    val dateEnd = end.toPatternDate(datePattern)
                    val days = eventController.getDaysWithEventsUser(user.id, dateStart, dateEnd).map{
                        it.toPatternString(datePattern)
                    }
                    call.respondAnswer(days)
                }else{
                    call.respondCodeResponse(CallbackCodeResponse.DATE_INCORRECT_DATE)
                }
            }catch (e: NullPointerException){
                call.respondNullExceptionErrorQueryParameters(e, listOf("start"))
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }

        get(API.event) {
            try {
                val id = call.request.queryParameters["id_event"]!!.toInt()
                val event = eventController.getEvent(id)
                if (event != null) {
                    val idsMentors = eventController.getIdsMentorsEvent(id)
                    val mentors = userController.getUsers(idsMentors).map {it.toSafe()}
                    val students = eventController.getStudentsEvent(id)
                    val studentsExist = userController.getUsers(students.filter{it.attended}.map {it.idUser}).map{ it.toSafe() }
                    val studentNotExist = userController.getUsers(students.filter{!it.attended}.map {it.idUser}).map{ it.toSafe() }
                    val safeFullEvent = event.toSafeFullEvent(mentors, studentsExist, studentNotExist)
                    call.respondAnswer(safeFullEvent)
                }else{
                    call.respondCodeResponse(CallbackCodeResponse.EVENT_NOT_FOUND)
                }
            }catch (e: NullPointerException){
                call.respondNullExceptionErrorQueryParameters(e, listOf("id_event"))
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }
    }
}
