package com.cit.plugins

import com.cit.common.Prefixes.Companion.API
import com.cit.controllers.MaterialController
import com.cit.respondAnswer
import com.cit.respondExceptionError
import com.cit.respondNullExceptionErrorQueryParameters
import io.ktor.server.application.*
import io.ktor.server.routing.*

fun Application.configureMaterialsRouting(){

    val materialController = MaterialController()

    routing {
        get(API.materialsEvent){
            try {
                val idEvent = call.request.queryParameters["id_event"]!!.toInt()
                val materialsEvent = materialController.getMaterialsEvent(idEvent)
                call.respondAnswer(materialsEvent)
            }catch (e: NullPointerException){
                call.respondNullExceptionErrorQueryParameters(e, listOf("id_event"))
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }

        get(API.materials){
            try {
                val materials = materialController.getMaterials()
                call.respondAnswer(materials)
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }
    }
}