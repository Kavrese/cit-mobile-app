package com.cit.plugins

import com.cit.common.Prefixes.Companion.API
import com.cit.common.models.LoginBody
import com.cit.common.models.SignUpBody
import com.cit.controllers.LoginController
import com.cit.receiveAndValidate
import com.cit.respondAnswer
import io.ktor.server.routing.*
import io.ktor.server.application.*

fun Application.configureLoginRouting() {

    val loginController = LoginController()
    routing {
        post(API.login){
            val body = call.receiveAndValidate(LoginBody::class)
            if (body != null)
                call.respondAnswer(loginController.login(body))
        }

        post(API.signUp){
            val bodySignUp = call.receiveAndValidate(SignUpBody::class)
            if (bodySignUp != null)
                call.respondAnswer(loginController.signup(bodySignUp))
        }
    }
}
