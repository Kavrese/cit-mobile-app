package com.cit.plugins

import com.cit.*
import com.cit.common.CallbackCodeResponse
import com.cit.common.Prefixes.Companion.MENTOR_API
import com.cit.common.Prefixes.Companion.STUDENT_API
import com.cit.common.models.ModelAnswer
import com.cit.common.models.ModelCompleteHomeTask
import com.cit.common.validateRequirementsHomeTask
import com.cit.controllers.HomeTaskController
import com.cit.controllers.UserController
import io.ktor.server.application.*
import io.ktor.server.routing.*

fun Application.configureHomeTaskRouting() {

    val userController = UserController()
    val homeTaskController = HomeTaskController()

    routing {
        get(STUDENT_API.homeTasks){
            try {
                val idUser = userController.getUserFromCallToken(call)?.id ?: return@get
                val homeTasks = homeTaskController.getHomeTasksUser(idUser)
                call.respondAnswer(ModelAnswer(homeTasks))
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }

        get(STUDENT_API.homeTask){
            try {
                val idUser = userController.getUserFromCallToken(call)?.id ?: return@get
                val id = call.request.queryParameters["id"]!!.toInt()
                val homeTask = homeTaskController.getHomeTaskStudent(idUser, id)
                call.respondAnswer(homeTask)
            }catch(e: NullPointerException){
                call.respondNullExceptionErrorQueryParameters(e, listOf("id"))
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }

        post(STUDENT_API.completeHomeTask){
            try {
                val body = call.receiveAndValidate(ModelCompleteHomeTask::class) ?: return@post
                val user = userController.getUserFromCallToken(call) ?: return@post
                if (!homeTaskController.checkExistHomeTask(body.idHomeTask)) {
                    call.respondCodeResponse(CallbackCodeResponse.HOMETASK_NOT_FOUND)
                    return@post
                }
                if (!homeTaskController.checkHomeTaskForUser(user.id, body.idHomeTask)){
                    call.respondCodeResponse(CallbackCodeResponse.HOMETASK_NOT_FOR_YOU)
                    return@post
                }
                if (homeTaskController.checkHomeTaskAlreadyInChecking(user.id, body.idHomeTask)){
                    call.respondCodeResponse(CallbackCodeResponse.HOMETASK_ALREADY_CHECKING)
                    return@post
                }
                val requirementsOriginHomeTask = homeTaskController.getRequirementsHomeTask(body.idHomeTask)
                if (!body.requirements.validateRequirementsHomeTask(requirementsOriginHomeTask)){
                    call.respondCodeResponse(CallbackCodeResponse.REQUIREMENT_NOT_CORRECT)
                }
                val answer = homeTaskController.toCheckHomeTask(user.id, body)
                call.respondAnswer(answer)
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }

        get(MENTOR_API.homeTasks){
            try {
                val mentor = userController.getUserMentorFromCallToken(call) ?: return@get
                val homeTasks = homeTaskController.getHomeTaskMentor(mentor.id)
                val statistics = homeTasks.map {
                    val students = homeTaskController.getUsersWithHomeTask(it.id)
                    homeTaskController.getStatisticStudentStatusHomeTask(it.id, students)
                }
                call.respondAnswer(statistics)
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }
    }
}