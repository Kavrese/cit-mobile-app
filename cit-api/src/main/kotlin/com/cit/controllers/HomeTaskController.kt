package com.cit.controllers

import com.cit.common.CallbackCodeResponse
import com.cit.common.StatusHomeTask
import com.cit.common.StatusHomeTask.Companion.toStatusHomeTask
import com.cit.common.models.ModelAnswer
import com.cit.common.models.ModelCompleteHomeTask
import com.cit.database.tables.hometasks.*
import com.cit.database.tables.hometasks_requirements.DAOHomeTaskRequirements
import com.cit.database.tables.hometasks_requirements.HomeTaskRequirements
import com.cit.database.tables.hometasks_requirements_students.DAOHomeTaskRequirementStudents
import com.cit.database.tables.hometasks_requirements_students.HomeTaskRequirementStudent
import com.cit.database.tables.hometasks_requirements_students.HomeTaskRequirementsStudents
import com.cit.database.tables.hometasks_students.DAOHomeTaskStudents
import com.cit.database.tables.hometasks_students.HomeTaskStudent
import com.cit.database.tables.hometasks_students.HomeTaskStudents
import com.cit.database.tables.users.DAOUsers
import com.cit.database.tables.users.Profile
import com.cit.database.tables.users.SafeProfile
import com.cit.database.tables.users.Users
import org.jetbrains.exposed.sql.and

class HomeTaskController {
    private val daoHomeTask = DAOHomeTasks()
    private val daoHomeTaskRequirement = DAOHomeTaskRequirements()
    private val daoHomeTaskRequirementStudents = DAOHomeTaskRequirementStudents()
    private val daoHomeTaskStudent = DAOHomeTaskStudents()
    private val daoUser = DAOUsers()

    suspend fun getHomeTaskStudent(idUser: Int, id: Int): ModelAnswer<SafeHomeTask>{

        val homeTask = daoHomeTask.selectSingle {
            HomeTasks.id eq id
        } ?: return ModelAnswer(CallbackCodeResponse.HOMETASK_NOT_FOUND)

        val status = daoHomeTaskStudent.selectSingle {
            (HomeTaskStudents.idUser eq idUser) and (HomeTaskStudents.idHomeTask eq id)
        }?.status ?: return ModelAnswer(CallbackCodeResponse.HOMETASK_NOT_FOR_YOU)

        val author = daoUser.selectSingle {
            Users.id eq homeTask.idAuthor
        }?.toSafe() ?: return ModelAnswer(CallbackCodeResponse.HOMETASK_AUTHOR_NOT_FOUND)

        val requirements = daoHomeTaskRequirement.selectMany {
            HomeTaskRequirements.idHomeTask eq homeTask.id
        }.map { it.requirement }

        val fullHomeTask = homeTask.toSafe(author, requirements, status)
        return ModelAnswer(fullHomeTask)
    }

    suspend fun notAcceptCheckHomeTask(idUser: Int, idHomeTask: Int): Boolean{
        daoHomeTaskRequirementStudents.delete {
            (HomeTaskRequirementsStudents.idUser eq idUser) and (HomeTaskRequirementsStudents.idHomeTask eq idHomeTask)
        }
        return changeStatusHomeTask(StatusHomeTask.IN_PROCESS, idUser, idHomeTask)
    }

    suspend fun toCheckHomeTask(idUser: Int, modelCompleteHomeTask: ModelCompleteHomeTask): Boolean{

        modelCompleteHomeTask.requirements.forEach { req ->
            daoHomeTaskRequirementStudents.insert(
                HomeTaskRequirementStudent(-1, idUser, req, modelCompleteHomeTask.idHomeTask)
            )
        }

        return changeStatusHomeTask(StatusHomeTask.IN_CHECKING, idUser, modelCompleteHomeTask.idHomeTask)
    }

    suspend fun toCompleteHomeTask(idUser: Int, modelCompleteHomeTask: ModelCompleteHomeTask): Boolean{
        return changeStatusHomeTask(StatusHomeTask.COMPLETE, idUser, modelCompleteHomeTask.idHomeTask)
    }

    suspend fun checkHomeTaskAlreadyInChecking(idUser: Int, idHomeTask: Int): Boolean{
        val row = daoHomeTaskStudent.selectSingle { (HomeTaskStudents.idUser eq idUser) and (HomeTaskStudents.idHomeTask eq idHomeTask) }
            ?: return false
        return row.status == StatusHomeTask.IN_CHECKING.title
    }

    private suspend fun changeStatusHomeTask(statusHomeTask: StatusHomeTask, idUser: Int, idHomeTask: Int): Boolean {
        return daoHomeTaskStudent.editStatus(statusHomeTask){
            (HomeTaskStudents.idHomeTask eq idHomeTask) and (HomeTaskStudents.idUser eq idUser)
        }
    }

    suspend fun getHomeTasksUser(idUser: Int): List<SafeHomeTask>{
        val idsHomeTasks = daoHomeTaskStudent.selectMany {
            HomeTaskStudents.idUser eq idUser
        }
        return idsHomeTasks.mapNotNull {
            getHomeTaskStudent(idUser, it.idHomeTask).body
        }
    }

    suspend fun getRequirementsHomeTask(idHomeTask: Int): List<String>{
        return daoHomeTaskRequirement.selectMany {
            HomeTaskRequirements.idHomeTask eq idHomeTask
        }.map { it.requirement }
    }

    suspend fun checkExistHomeTask(idHomeTask: Int): Boolean{
        return daoHomeTask.checkExist {
            HomeTasks.id eq idHomeTask
        }
    }

    suspend fun checkHomeTaskForUser(idUser: Int, idHomeTask: Int): Boolean{
        return daoHomeTaskStudent.checkExist {
            (HomeTaskStudents.idUser eq idUser) and (HomeTaskStudents.idHomeTask eq idHomeTask)
        }
    }

    suspend fun getUsersWithHomeTask(idHomeTask: Int): List<SafeProfile>{
        return daoHomeTaskStudent.selectMany {
            HomeTaskStudents.idHomeTask eq idHomeTask
        }.mapNotNull {
            daoUser.selectSingle { Users.id eq it.idUser }?.toSafe()
        }
    }

    suspend fun getStatusCompleteHomeTaskUser(idUser: Int, idHomeTask: Int): StatusHomeTask? {
        val userHomeTask = daoHomeTaskStudent.selectSingle {
            (HomeTaskStudents.idHomeTask eq idHomeTask) and (HomeTaskStudents.idUser eq idUser)
        } ?: return null
        return userHomeTask.status.toStatusHomeTask()
    }

    suspend fun getStatisticStudentStatusHomeTask(idHomeTask: Int, students: List<SafeProfile>): StatisticHomeTask{
        val statistic = StatisticHomeTask(idHomeTask, mutableListOf(), mutableListOf(), mutableListOf())
        students.forEach {
            val status = getStatusCompleteHomeTaskUser(it.id, idHomeTask)
            if (status != null){
                statistic.addStudentStatus(it, status)
            }
        }
        return statistic
    }

    suspend fun getHomeTaskMentor(idMentor: Int): List<HomeTask>{
        return daoHomeTask.selectMany {
            HomeTasks.idAuthor eq idMentor
        }
    }
}