package com.cit.controllers

import com.cit.common.CallbackCodeResponse
import com.cit.common.CodeResponse
import com.cit.common.TypeProfile
import com.cit.common.models.LoginBody
import com.cit.database.tables.users.DAOUsers
import com.cit.database.tables.users.Profile
import com.cit.database.tables.users.Users
import com.cit.respondCodeResponse
import com.cit.respondExceptionError
import com.cit.respondNullExceptionErrorQueryParameters
import io.ktor.server.application.*
import org.jetbrains.exposed.sql.and

class UserController {
    private val daoUsers = DAOUsers()

    suspend fun getUserFromCallToken(call: ApplicationCall): Profile? {
        try {
            val token = call.request.queryParameters["token"]!!
            val user = getUser(token)
            if (user == null){
                call.respondCodeResponse(CallbackCodeResponse.TOKEN_NOT_CORRECT)
                return null
            }
            return user
        } catch (e: NullPointerException) {
            call.respondNullExceptionErrorQueryParameters(e, listOf("token"))
        } catch (e: Exception) {
            call.respondExceptionError(e)
        }
        return null
    }

    suspend fun getUserMentorFromCallToken(call: ApplicationCall): Profile?{
        val user = getUserFromCallToken(call) ?: return null
        if (!user.type.checkLevelRange(listOf(TypeProfile.MENTOR, TypeProfile.MENTOR_ADMIN))){
            call.respondCodeResponse(CallbackCodeResponse.THIS_ENDPOINT_NOT_FOR_YOU)
            return null
        }
        return user
    }

    suspend fun getUser(idUser: Int): Profile?{
        return daoUsers.selectSingle { Users.id eq idUser }
    }

    suspend fun getUser(token: String): Profile?{
        return daoUsers.selectSingle { Users.token eq token }
    }

    suspend fun getUsers(ids: List<Int>): List<Profile>{
        return daoUsers.selectMany {
            Users.id.inList(ids)
        }
    }

    suspend fun getUser(loginBody: LoginBody): Profile? {
        return daoUsers.selectSingle {
            (Users.login eq loginBody.login) and (Users.password eq loginBody.password)
        }
    }

    suspend fun editToken(idUser: Int, token: String): Boolean{
        return daoUsers.editToken(token) {
            Users.id eq idUser
        }
    }

    suspend fun checkToken(token: String): Boolean{
        return daoUsers.editToken(token) {
            Users.token eq token
        }
    }

    suspend fun existUser(idUser: Int): Boolean{
        return daoUsers.checkExist {
            Users.id eq idUser
        }
    }

    suspend fun existUser(login: String): Boolean{
        return daoUsers.checkExist {
            (Users.login eq login)
        }
    }

    suspend fun insertNewProfile(profile: Profile): Profile?{
        return daoUsers.insert(profile)
    }
}