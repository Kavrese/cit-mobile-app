package com.cit.controllers

import com.cit.common.datePattern
import com.cit.common.models.ModelDay
import com.cit.common.toPatternString
import com.cit.database.tables.events.DAOEvents
import com.cit.database.tables.events.Event
import com.cit.database.tables.events.Events
import com.cit.database.tables.events.SafeEventShort
import com.cit.database.tables.mentors_events.DAOMentorsMentorEvents
import com.cit.database.tables.mentors_events.MentorsEvents
import com.cit.database.tables.students_events.DAOStudentsStudentEvents
import com.cit.database.tables.students_events.StudentEvent
import com.cit.database.tables.students_events.StudentsEvents
import org.jetbrains.exposed.sql.and
import java.time.LocalDate

class EventController {
    private val daoEvents = DAOEvents()
    private val daoMentorsMentorEvents = DAOMentorsMentorEvents()
    private val daoStudentsStudentEvents = DAOStudentsStudentEvents()

    suspend fun getUserEventsDay(idUser: Int, date: LocalDate): List<SafeEventShort>{
        return daoEvents.selectMany { Events.date eq date }.filter {
            checkExistUserEvent(idUser, it.id)
        }.map {
            it.toSafeShort()
        }
    }

    suspend fun getUserEvents(idUser: Int): List<SafeEventShort>{
        return (getStudentEvents(idUser) + getMentorsEvents(idUser)).toSet().toList()
    }

    suspend fun getStudentEvents(idStudent: Int): List<SafeEventShort>{
        return daoStudentsStudentEvents.selectMany {
            StudentsEvents.idUser eq idStudent
        }.mapNotNull {
            getEvent(it.idEvent)?.toSafeShort()
        }
    }

    suspend fun getMentorsEvents(idMentor: Int): List<SafeEventShort>{
        return daoMentorsMentorEvents.selectMany {
            MentorsEvents.idUser eq idMentor
        }.mapNotNull {
            getEvent(it.idEvent)?.toSafeShort()
        }
    }

    suspend fun checkExistUserEvent(idUser: Int, idEvent: Int): Boolean{
        return daoStudentsStudentEvents.checkExist {
            (StudentsEvents.idUser eq idUser) and (StudentsEvents.idEvent eq idEvent)
        } || daoMentorsMentorEvents.checkExist {
            (MentorsEvents.idUser eq idUser) and (MentorsEvents.idEvent eq idEvent)
        }
    }

    suspend fun getDaysWithEventsUser(idUser: Int, start: LocalDate, end: LocalDate): List<LocalDate>{
        return daoEvents
            .selectMany{ Events.date.between(start, end) }
            .filter { checkExistUserEvent(idUser, it.id) }
            .map { it.date }.toSet().toList()
    }

    suspend fun getUserEventsDays(days: List<LocalDate>, idUser: Int): List<ModelDay<SafeEventShort>>{
        return days.map { ModelDay(it.toPatternString(datePattern), getUserEventsDay(idUser, it)) }
    }

    suspend fun getIdsMentorsEvent(idEvent: Int): List<Int>{
        return daoMentorsMentorEvents.selectMany {
            MentorsEvents.idEvent eq idEvent
        }.map { it.idUser }
    }

    suspend fun getStudentsEvent(idEvent: Int): List<StudentEvent> {
        return daoStudentsStudentEvents.selectMany {
            StudentsEvents.idEvent eq idEvent
        }
    }

    suspend fun getEvent(idEvent: Int): Event?{
        return daoEvents.selectSingle { Events.id eq idEvent }
    }
}