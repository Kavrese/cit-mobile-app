package com.cit.controllers

import com.cit.common.CallbackCodeResponse
import com.cit.common.models.LoginBody
import com.cit.common.models.ModelAnswer
import com.cit.common.models.SignUpBody
import com.cit.database.tables.users.Token
import java.util.UUID

class LoginController {
    private val userController: UserController = UserController()

    suspend fun login(loginBody: LoginBody): ModelAnswer<Token>{
        val user = userController.getUser(loginBody)
        if (user != null){
            val token = UUID.randomUUID().toString()
            val res = userController.editToken(user.id, token)
            return if (res) ModelAnswer(Token(token)) else ModelAnswer(CallbackCodeResponse.NEW_TOKEN_NOT_INSERT)
        }
        return ModelAnswer(CallbackCodeResponse.USER_NOT_FOUND)
    }

    suspend fun signup(signUpBody: SignUpBody): ModelAnswer<Token>{
        if (userController.existUser(signUpBody.login))
            return ModelAnswer(CallbackCodeResponse.LOGIN_ALREADY_EXIST)
        val token = UUID.randomUUID().toString()
        val user = userController.insertNewProfile(signUpBody.toProfile(token))
        return if (user == null)
            ModelAnswer(CallbackCodeResponse.USER_NOT_CREATED)
        else
            ModelAnswer(user.toToken())
    }
}