package com.cit.controllers

import com.cit.common.CallbackCodeResponse
import com.cit.common.models.ModelAnswer
import com.cit.database.tables.materials.DAOMaterials
import com.cit.database.tables.materials.Materials
import com.cit.database.tables.materials.SafeMaterial
import com.cit.database.tables.materials_events.DAOMaterialsEvents
import com.cit.database.tables.materials_events.MaterialsEvents

class MaterialController {
    private val daoMaterials = DAOMaterials()
    private val daoMaterialsEvents = DAOMaterialsEvents()
    private val userController = UserController()

    suspend fun getMaterial(id: Int): ModelAnswer<SafeMaterial>{
        val material = daoMaterials.selectSingle {
            Materials.id eq id
        } ?: return ModelAnswer(CallbackCodeResponse.MATERIAL_NOT_FOUND)
        val author = userController.getUser(material.idAuthor)?.toSafe() ?: return ModelAnswer(CallbackCodeResponse.AUTHOR_NOT_FOUND)
        return ModelAnswer(material.toSafe(author))
    }

    suspend fun getMaterialsEvent(idEvent: Int): ModelAnswer<List<SafeMaterial>>{
        val materialsIds = daoMaterialsEvents.selectMany { MaterialsEvents.idEvent eq idEvent }.map { it.idMaterial }
        return ModelAnswer(materialsIds.mapNotNull {
            getMaterial(it).body
        })
    }

    suspend fun getMaterials(): ModelAnswer<List<SafeMaterial>>{
        val materials = daoMaterials.selectAll().mapNotNull {
            val author = userController.getUser(it.idAuthor)?.toSafe() ?: return@mapNotNull null
            it.toSafe(author)
        }
        return ModelAnswer(materials)
    }
}