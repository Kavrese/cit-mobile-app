package com.cit.common


enum class TypeProfile(val title: String, val level: Int) {
    STUDENT("student", 3),
    MENTOR("mentor", 2),
    MENTOR_ADMIN("mentor_admin", 1),
    ADMIN("admin", 1);

    companion object {
        fun String.toTypeProfile(): TypeProfile{
            return values()[values().map { it.title }.indexOf(this)]
        }
    }

    fun checkLevel(level: Int): Boolean{
        return this.level >= level
    }

    fun checkLevel(level: TypeProfile): Boolean{
        return checkLevel(level.level)
    }

    fun checkLevelBetween(levelStart: Int, levelEnd: Int): Boolean{
        return (levelStart >= this.level) && (this.level >= levelEnd)
    }

    fun checkLevelBetween(levelStart: TypeProfile, levelEnd: TypeProfile): Boolean{
        return checkLevelBetween(levelStart.level, levelEnd.level)
    }

    @JvmName("checkLevelRangeTypeProfile")
    fun checkLevelRange(range: Collection<TypeProfile>): Boolean{
        return checkLevelRange(range.map{it.level})
    }

    @JvmName("checkLevelRangeInt")
    fun checkLevelRange(range: Collection<Int>): Boolean{
        return this.level in range
    }
}