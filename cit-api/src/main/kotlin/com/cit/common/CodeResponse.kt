package com.cit.common

import com.cit.common.models.ModelAnswer
import io.ktor.http.*

/*
    Custom code errors:
        10xx - Общие ошибки
            1000 - NEW_TOKEN_NOT_CREATED / Новый токен не был присвоен пользователю
        12xx - Ошибки регистрации:
            1200 - USER_NOT_CREATED / Пользователь не был создан
*/

@kotlinx.serialization.Serializable
class CodeResponse {
    val code: Int
    val description: String

    constructor(code: Int): this(code, "")
    constructor(httpStatusCode: HttpStatusCode, description: String): this(httpStatusCode.value, description)
    constructor(httpStatusCode: HttpStatusCode, codeTitle: CodeTitle): this(httpStatusCode.value, codeTitle)
    constructor(code: Int, codeTitle: CodeTitle): this(code, codeTitle.toString())
    constructor(httpStatusCode: HttpStatusCode): this(httpStatusCode.value, httpStatusCode.description)
    constructor(code: Int, description: String){
        this.description = description
        this.code = code
    }

    fun convertToHttpsStatusCode(): HttpStatusCode{
        return HttpStatusCode(code, description)
    }

    fun <T> toModelAnswer(body: T?=null): ModelAnswer<T> {
        return ModelAnswer(this, body)
    }

    companion object {
        fun HttpStatusCode.toCodeResponse(): CodeResponse {
            return CodeResponse(this)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other is HttpStatusCode) return code == other.value
        return super.equals(other)
    }
    /*
    Было созданно автоматически IDEей
     */
    override fun hashCode(): Int {
        var result = code
        result = 31 * result + description.hashCode()
        return result
    }
}

enum class CodeTitle(private val title: String){
    USER_NOT_FOUND("User not found"),
    FIRSTNAME_EMPTY("Firstname empty"),
    LASTNAME_EMPTY("Lastname empty"),
    PATRONYMIC_EMPTY("Patronymic empty"),
    FILE_NOT_FOUND("File not found"),
    AUTHOR_NOT_FOUND("Author not found"),
    EVENT_NOT_FOUND("Event not found"),
    PASSWORD_8("Password not correct (min 8 length)"),
    REQUIREMENT_NOT_CORRECT("Single or many requirements not correct"),
    LOGIN_ALREADY_EXIST("This login already exist"),
    LOGIN_EMPTY("Login empty"),
    DATE_INCORRECT_DATE("Date incorrect ($datePattern)"),
    DATE_INCORRECT_TIME("Time incorrect ($timePattern)"),
    DATE_INCORRECT("Date incorrect ($fullPattern)"),
    USER_NOT_CREATED("User not created"),
    TYPE_UNKNOWN("This type not exist"),
    NEW_TOKEN_NOT_INSERT("New token not created"),
    MASTER_PASSWORD_NOT_CORRECT("Master password not correct"),
    HOMETASK_NOT_FOUND("Home task not found"),
    HOMETASK_AUTHOR_NOT_FOUND("Author home task not found"),
    TOKEN_NOT_CORRECT("Token not correct"),
    HOMETASK_NOT_FOR_YOU("This home task not for you"),
    HOMETASK_ALREADY_CHECKING("This home task already checking"),
    MATERIAL_NOT_FOUND("Material not found"),
    THIS_ENDPOINT_NOT_FOR_YOU("This endpoint not for this user");

    override fun toString(): String {
        return title
    }
}

enum class CallbackCodeResponse(val codeResponse: CodeResponse){
    NEW_TOKEN_NOT_INSERT(CodeResponse(1000, CodeTitle.NEW_TOKEN_NOT_INSERT)),
    TOKEN_NOT_CORRECT(CodeResponse(HttpStatusCode.BadRequest, CodeTitle.TOKEN_NOT_CORRECT)),

    USER_NOT_CREATED(CodeResponse(1200, CodeTitle.USER_NOT_CREATED)),

    USER_NOT_FOUND(CodeResponse(HttpStatusCode.NotFound, CodeTitle.USER_NOT_FOUND)),
    FILE_NOT_FOUND(CodeResponse(HttpStatusCode.NotFound, CodeTitle.FILE_NOT_FOUND)),
    AUTHOR_NOT_FOUND(CodeResponse(HttpStatusCode.NotFound, CodeTitle.AUTHOR_NOT_FOUND)),
    EVENT_NOT_FOUND(CodeResponse(HttpStatusCode.NotFound, CodeTitle.EVENT_NOT_FOUND)),
    HOMETASK_NOT_FOUND(CodeResponse(HttpStatusCode.NotFound, CodeTitle.HOMETASK_NOT_FOUND)),
    HOMETASK_AUTHOR_NOT_FOUND(CodeResponse(HttpStatusCode.NotFound, CodeTitle.HOMETASK_AUTHOR_NOT_FOUND)),

    LOGIN_ALREADY_EXIST(CodeResponse(HttpStatusCode.NotAcceptable, CodeTitle.LOGIN_ALREADY_EXIST)),

    MASTER_PASSWORD_NOT_CORRECT(CodeResponse(HttpStatusCode.BadRequest, CodeTitle.MASTER_PASSWORD_NOT_CORRECT)),

    PASSWORD_NOT_CORRECT_8(CodeResponse(HttpStatusCode.BadRequest, CodeTitle.PASSWORD_8)),

    DATE_INCORRECT_DATE(CodeResponse(HttpStatusCode.BadRequest, CodeTitle.DATE_INCORRECT_DATE)),
    DATE_INCORRECT_TIME(CodeResponse(HttpStatusCode.BadRequest, CodeTitle.DATE_INCORRECT_TIME)),
    DATE_INCORRECT(CodeResponse(HttpStatusCode.BadRequest, CodeTitle.DATE_INCORRECT)),

    THIS_ENDPOINT_NOT_FOR_YOU(CodeResponse(HttpStatusCode.BadRequest, CodeTitle.THIS_ENDPOINT_NOT_FOR_YOU)),

    REQUIREMENT_NOT_CORRECT(CodeResponse(HttpStatusCode.BadRequest, CodeTitle.REQUIREMENT_NOT_CORRECT)),
    HOMETASK_NOT_FOR_YOU(CodeResponse(HttpStatusCode.BadRequest, CodeTitle.HOMETASK_NOT_FOR_YOU)),
    HOMETASK_ALREADY_CHECKING(CodeResponse(HttpStatusCode.BadRequest, CodeTitle.HOMETASK_ALREADY_CHECKING)),
    MATERIAL_NOT_FOUND(CodeResponse(HttpStatusCode.NotFound, CodeTitle.MATERIAL_NOT_FOUND)),
}
