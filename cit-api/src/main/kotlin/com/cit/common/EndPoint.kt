package com.cit.common

class Prefixes{
    companion object{
        val API = EndPoints("api")
        val MENTOR_API = EndPoints("${API.prefix}/mentor")
        val STUDENT_API = EndPoints("${API.prefix}/student")
    }
}

open class EndPoints(val prefix: String){
    val login = "$prefix/login"
    val signUp = "$prefix/sign_up"
    val events = "$prefix/events"
    val daysWithEventData = "$prefix/days_with_event_data"
    val eventsDay = "$prefix/events_day"
    val daysWithEvent = "$prefix/days_with_event"
    val event = "$prefix/event"
    val homeTasks = "$prefix/hometasks"
    val homeTask = "$prefix/hometask"
    val completeHomeTask = "$prefix/complete_hometask"
    val materialsEvent = "$prefix/materials_event"
    val materials = "$prefix/materials"
    val profile = "$prefix/profile"
    val me = "$prefix/me"
}