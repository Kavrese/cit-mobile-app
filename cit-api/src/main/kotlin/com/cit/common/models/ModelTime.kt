package com.cit.common.models

@kotlinx.serialization.Serializable
data class ModelTime <T> (
    val time: String,
    val events: List<T>
)
