package com.cit.common.models

import com.cit.common.CodeTitle
import com.cit.common.Validation
import com.cit.common.validatePassword

@kotlinx.serialization.Serializable
data class LoginBody (
    val login: String,
    val password: String
): Validation{
    override fun validate(): Pair<Boolean, String?> {
        if (!password.validatePassword())
            return Pair(false, CodeTitle.PASSWORD_8.toString())
        if (login.isEmpty())
            return Pair(false, CodeTitle.LOGIN_EMPTY.toString())
        return Pair(true, null)
    }
}