package com.cit.common.models

import com.cit.common.CodeTitle
import com.cit.common.Validation
import com.cit.common.validateRequirement
import kotlinx.serialization.Serializable

@Serializable
data class ModelCompleteHomeTask(
    val idHomeTask: Int,
    val requirements: List<String>
): Validation{
    override fun validate(): Pair<Boolean, String?> {
        if (requirements.any { !it.validateRequirement() })
            return Pair(false, CodeTitle.REQUIREMENT_NOT_CORRECT.toString())
        return Pair(true, null)
    }
}
