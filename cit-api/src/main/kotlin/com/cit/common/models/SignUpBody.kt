package com.cit.common.models

import com.cit.common.CodeTitle
import com.cit.common.TypeProfile.Companion.toTypeProfile
import com.cit.common.Validation
import com.cit.common.validatePassword
import com.cit.common.validateType
import com.cit.database.tables.users.Profile

@kotlinx.serialization.Serializable
data class SignUpBody(
    val firstname: String,
    val lastname: String,
    val patronymic: String,
    val type: String,
    val login: String,
    val password: String,
    val avatar: String = "default"
): Validation {

    fun toLoginBody(): LoginBody = LoginBody(login, password)
    fun toProfile(token: String): Profile = Profile(
        -1, login, firstname, lastname, patronymic, password, token, type.toTypeProfile(), avatar
    )

    override fun validate(): Pair<Boolean, String?> {
        if (firstname.isEmpty())
            return Pair(false, CodeTitle.FIRSTNAME_EMPTY.toString())
        if (lastname.isEmpty())
            return Pair(false, CodeTitle.LASTNAME_EMPTY.toString())
        if (patronymic.isEmpty())
            return Pair(false, CodeTitle.PATRONYMIC_EMPTY.toString())
        if (!type.validateType())
            return Pair(false, CodeTitle.TYPE_UNKNOWN.toString())
        if (!password.validatePassword())
            return Pair(false, CodeTitle.PASSWORD_8.toString())
        if (login.isEmpty())
            return Pair(false, CodeTitle.LOGIN_EMPTY.toString())
        return Pair(true, null)
    }
}