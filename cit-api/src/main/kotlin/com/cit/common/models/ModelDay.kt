package com.cit.common.models

@kotlinx.serialization.Serializable
data class ModelDay <T>(
    val date: String,
    val events: List<T>
)
