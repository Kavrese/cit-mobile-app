package com.cit.common


fun String.validateEmail(): Boolean{
    return ("@" in this) && (count { it == '.' } == 1)
}

fun String.validatePassword(): Boolean{
    return this.length >= 8
}

fun String.validateType(): Boolean{
    return this in TypeProfile.values().map {
        it.title
    }
}

fun String.validateDate(pattern: String): Boolean{
    return try {
        toPatternDate(pattern)
        true
    }catch (e: Exception){
        false
    }
}

fun String.validateTime(pattern: String): Boolean{
    return try {
        toPatternTime(pattern)
        true
    }catch (e: Exception){
        false
    }
}

fun String.validateRequirement(): Boolean{
    return isNotEmpty()
}

fun Collection<String>.validateRequirementsHomeTask(requirements: List<String>): Boolean{
    return this.size == requirements.size
}

interface Validation{
    fun validate(): Pair<Boolean, String?>
}