package com.cit.common

enum class StatusHomeTask(val title: String) {
    IN_PROCESS("В процессе выполнения"),
    COMPLETE("Выполнено"),
    IN_CHECKING("На проверке"),
    DEADLINE("Просрочено");

    fun getAllStatusTitle(): List<String>{
        return values().map{it.title}
    }

    companion object {
        fun String.toStatusHomeTask(): StatusHomeTask?{
            return try {
                values().filter { it.title == this }[0]
            }catch (e: IndexOutOfBoundsException){
                null
            }
        }
    }
}