package com.cit.database

import com.cit.database.tables.events.Events
import com.cit.database.tables.hometasks.HomeTasks
import com.cit.database.tables.hometasks_requirements.HomeTaskRequirements
import com.cit.database.tables.hometasks_students.HomeTaskStudents
import com.cit.database.tables.materials.Materials
import com.cit.database.tables.materials_events.MaterialsEvents
import com.cit.database.tables.mentors_events.MentorsEvents
import com.cit.database.tables.students_events.StudentsEvents
import com.cit.database.tables.users.Users
import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction

object DatabaseFactory{
    fun initDataBase(){
        val driverClassName = "org.postgresql.Driver"
        val url = "jdbc:postgresql://localhost:5432/cit"
        val user = "postgres"
        val password = "QwE123asd"
        val database = Database.connect(url, driverClassName, user, password)

        createTablesIfNotExist(database)
    }

    private fun createTablesIfNotExist(database: Database){
        transaction(database){
            SchemaUtils.create(Users)
            SchemaUtils.create(Events)
            SchemaUtils.create(HomeTasks)
            SchemaUtils.create(HomeTaskRequirements)
            SchemaUtils.create(MentorsEvents)
            SchemaUtils.create(StudentsEvents)
            SchemaUtils.create(HomeTaskStudents)
            SchemaUtils.create(Materials)
            SchemaUtils.create(MaterialsEvents)
            SchemaUtils.create(MentorsEvents)
            SchemaUtils.create(StudentsEvents)
        }
    }

    suspend fun <T> pushQuery(block: suspend () -> T): T{
        return newSuspendedTransaction(Dispatchers.IO) { block() }
    }
}