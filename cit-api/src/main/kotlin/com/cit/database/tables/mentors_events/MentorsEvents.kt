package com.cit.database.tables.mentors_events

import org.jetbrains.exposed.sql.Table

data class MentorEvent(
    val id: Int,
    val idUser: Int,
    val idEvent: Int
)

object MentorsEvents: Table("events_mentors") {
    val id = integer("id").autoIncrement()
    val idUser = integer("id_user")
    val idEvent = integer("id_event")

    override val primaryKey: PrimaryKey = PrimaryKey(id)
}