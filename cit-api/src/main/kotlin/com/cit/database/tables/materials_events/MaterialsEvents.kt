package com.cit.database.tables.materials_events

import com.cit.database.tables.materials.TypeMaterial.Companion.toTypeMaterial
import com.cit.database.tables.users.SafeProfile
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table

data class MaterialEvent(
    val id: Int,
    val idEvent: Int,
    val idMaterial: Int
)

object MaterialsEvents: Table("materials_events"){
    val id = integer("id").autoIncrement()
    val idEvent = integer("id_event")
    val idMaterial = integer("id_material")

    override val primaryKey: PrimaryKey = PrimaryKey(id)
}