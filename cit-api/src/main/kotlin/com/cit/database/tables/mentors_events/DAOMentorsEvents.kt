package com.cit.database.tables.mentors_events


import com.cit.database.DAOTable
import com.cit.database.DatabaseFactory.pushQuery
import org.jetbrains.exposed.sql.*

class DAOMentorsMentorEvents: DAOTable<MentorEvent> {
    override fun resultRowToModel(row: ResultRow): MentorEvent {
        return MentorEvent(
            id = row[MentorsEvents.id],
            idUser = row[MentorsEvents.idUser],
            idEvent = row[MentorsEvents.idEvent]
        )
    }

    override suspend fun selectAll(): List<MentorEvent> {
        return pushQuery{
            MentorsEvents.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): MentorEvent? {
        return selectMany(where).singleOrNull()
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<MentorEvent> {
        return pushQuery {
            MentorsEvents.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            MentorsEvents.deleteWhere(op = where) > 0
        }
    }

    override suspend fun checkExist(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return selectSingle(where) != null
    }

    override suspend fun insert(model: MentorEvent): MentorEvent? {
        return pushQuery {
            MentorsEvents.insert {
                it[idUser] = model.idUser
                it[idEvent] = model.idEvent
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun edit(model: MentorEvent, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            MentorsEvents.update(where){
                it[idUser] = model.idUser
                it[idEvent] = model.idEvent
            } > 0
        }
    }
}