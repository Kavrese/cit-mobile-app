package com.cit.database.tables.materials


import com.cit.database.DAOTable
import com.cit.database.DatabaseFactory.pushQuery
import com.cit.database.tables.materials.TypeMaterial.Companion.toTypeMaterial
import org.jetbrains.exposed.sql.*

class DAOMaterials: DAOTable<Material> {
    override fun resultRowToModel(row: ResultRow): Material {
        return Material(
            id = row[Materials.id],
            title = row[Materials.title],
            href = row[Materials.href],
            cover = row[Materials.cover],
            type =  row[Materials.type].toTypeMaterial(),
            idAuthor = row[Materials.idAuthor]
        )
    }

    override suspend fun selectAll(): List<Material> {
        return pushQuery{
            Materials.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): Material? {
        return selectMany(where).singleOrNull()
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<Material> {
        return pushQuery {
            Materials.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            Materials.deleteWhere(op = where) > 0
        }
    }

    override suspend fun checkExist(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return selectSingle(where) != null
    }

    override suspend fun insert(model: Material): Material? {
        return pushQuery {
            Materials.insert {
                it[title] = model.title
                it[href] = model.href
                it[cover] = model.cover
                it[type] = model.type.toString()
                it[idAuthor] = model.idAuthor
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun edit(model: Material, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            Materials.update(where){
                it[title] = model.title
                it[href] = model.href
                it[cover] = model.cover
                it[type] = model.type.toString()
                it[idAuthor] = model.idAuthor
            } > 0
        }
    }
}