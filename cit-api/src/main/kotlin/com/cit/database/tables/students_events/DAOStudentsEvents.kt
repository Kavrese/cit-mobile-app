package com.cit.database.tables.students_events


import com.cit.database.DAOTable
import com.cit.database.DatabaseFactory.pushQuery
import org.jetbrains.exposed.sql.*

class DAOStudentsStudentEvents: DAOTable<StudentEvent> {
    override fun resultRowToModel(row: ResultRow): StudentEvent {
        return StudentEvent(
            id = row[StudentsEvents.id],
            idUser = row[StudentsEvents.idUser],
            idEvent = row[StudentsEvents.idEvent],
            attended = row[StudentsEvents.attended]
        )
    }

    override suspend fun selectAll(): List<StudentEvent> {
        return pushQuery{
            StudentsEvents.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): StudentEvent? {
        return selectMany(where).singleOrNull()
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<StudentEvent> {
        return pushQuery {
            StudentsEvents.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            StudentsEvents.deleteWhere(op = where) > 0
        }
    }

    override suspend fun checkExist(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return selectSingle(where) != null
    }

    override suspend fun insert(model: StudentEvent): StudentEvent? {
        return pushQuery {
            StudentsEvents.insert {
                it[idUser] = model.idUser
                it[idEvent] = model.idEvent
                it[attended] = model.attended
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun edit(model: StudentEvent, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            StudentsEvents.update(where){
                it[idUser] = model.idUser
                it[idEvent] = model.idEvent
                it[attended] = model.attended
            } > 0
        }
    }
}