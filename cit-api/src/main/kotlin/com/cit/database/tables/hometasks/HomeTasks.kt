package com.cit.database.tables.hometasks

import com.cit.common.StatusHomeTask
import com.cit.common.datePattern
import com.cit.common.toPatternString
import com.cit.database.tables.users.SafeProfile
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.javatime.date
import java.time.LocalDate

data class HomeTask(
    val id: Int,
    val date: LocalDate,
    val title: String,
    val deadline: LocalDate,
    val idEvent: Int,
    val idAuthor: Int
){
    fun toSafe(author: SafeProfile, requirements: List<String>, status: String): SafeHomeTask {
        return SafeHomeTask(
            id,
            date.toPatternString(datePattern),
            title,
            deadline.toPatternString(datePattern),
            idEvent,
            author,
            requirements,
            status
        )
    }
}

@kotlinx.serialization.Serializable
data class SafeHomeTask(
    val id: Int,
    val date: String,
    val title: String,
    val deadline: String,
    val idEvent: Int,
    val author: SafeProfile,
    val requirements: List<String>,
    val status: String
)


@Serializable
data class StatisticHomeTask(
    val idHomeTask: Int,
    val complete: MutableList<SafeProfile>,
    val inCheck: MutableList<SafeProfile>,
    val notComplete: MutableList<SafeProfile>,
){
    fun addStudentStatus(student: SafeProfile, status: StatusHomeTask){
        when (status) {
            StatusHomeTask.COMPLETE -> complete.add(student)
            StatusHomeTask.IN_CHECKING -> inCheck.add(student)
            StatusHomeTask.IN_PROCESS, StatusHomeTask.DEADLINE -> notComplete.add(student)
        }
    }
}

object HomeTasks: Table("hometasks") {
    val id = integer("id").autoIncrement()
    val date = date("date")
    val title = varchar("title", 5000)
    val deadline = date("deadline")
    val idEvent = integer("id_event")
    val idAuthor = integer("id_author")


    override val primaryKey: PrimaryKey = PrimaryKey(id)
}