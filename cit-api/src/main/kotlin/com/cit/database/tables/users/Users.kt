package com.cit.database.tables.users

import com.cit.common.TypeProfile
import org.jetbrains.exposed.sql.Table

data class Profile(
    val id: Int,
    val login: String,
    val firstname: String,
    val lastname: String,
    val patronymic: String,
    val password: String,
    val token: String,
    val type: TypeProfile,
    val avatar: String
){
    fun toSafe(): SafeProfile = SafeProfile(id, firstname, lastname, patronymic, type.title, avatar)
    fun toToken(): Token = Token(token)
}

@kotlinx.serialization.Serializable
data class Token(
    val token: String
)

@kotlinx.serialization.Serializable
data class SafeProfile(
    val id: Int,
    val firstname: String,
    val lastname: String,
    val patronymic: String,
    val type: String,
    val avatar: String
)

object Users: Table() {
    val id = integer("id").autoIncrement()
    val firstname = varchar("firstname", 50)
    val lastname = varchar("lastname", 50)
    val patronymic = varchar("patronymic", 50)
    val password = varchar("password", 1000)
    val token = varchar("token", 120)
    val type = varchar("type", 500)
    val avatar = varchar("avatar", 1000)
    val login = varchar("login", 1000)

    override val primaryKey = PrimaryKey(id)
}