package com.cit.database.tables.hometasks_students


import com.cit.common.StatusHomeTask
import com.cit.database.DAOTable
import com.cit.database.DatabaseFactory.pushQuery
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq

class DAOHomeTaskStudents: DAOTable<HomeTaskStudent> {
    override fun resultRowToModel(row: ResultRow): HomeTaskStudent {
        return HomeTaskStudent(
            id = row[HomeTaskStudents.id],
            idUser = row[HomeTaskStudents.idUser],
            idHomeTask = row[HomeTaskStudents.idHomeTask],
            status = row[HomeTaskStudents.status]
        )
    }

    override suspend fun selectAll(): List<HomeTaskStudent> {
        return pushQuery{
            HomeTaskStudents.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): HomeTaskStudent? {
        return selectMany(where).singleOrNull()
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<HomeTaskStudent> {
        return pushQuery {
            HomeTaskStudents.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            HomeTaskStudents.deleteWhere(op = where) > 0
        }
    }

    override suspend fun checkExist(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return selectSingle(where) != null
    }

    override suspend fun insert(model: HomeTaskStudent): HomeTaskStudent? {
        return pushQuery {
            HomeTaskStudents.insert {
                it[idUser] = model.idUser
                it[idHomeTask] = model.idHomeTask
                it[status] = model.status
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun edit(model: HomeTaskStudent, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            HomeTaskStudents.update(where){
                it[idUser] = model.idUser
                it[idHomeTask] = model.idHomeTask
                it[status] = model.status
            } > 0
        }
    }

    suspend fun editStatus(newStatus: StatusHomeTask, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
           HomeTaskStudents.update(where){
               it[status] = newStatus.title
           } > 0
        }
    }
}