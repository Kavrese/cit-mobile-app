package com.cit.database.tables.hometasks_requirements_students

import org.jetbrains.exposed.sql.Table

data class HomeTaskRequirementStudent(
    val id: Int,
    val idUser: Int,
    val requirementAnswer: String,
    val idHomeTask: Int
)

object HomeTaskRequirementsStudents: Table("hometasks_requirements_students") {
    val id = integer("id").autoIncrement()
    val idHomeTask = integer("id_hometask")
    val idUser = integer("id_user")
    val requirementAnswer = varchar("requirement_answer", 5000)

    override val primaryKey: PrimaryKey = PrimaryKey(id)
}