package com.cit.database.tables.events


import com.cit.database.DAOTable
import com.cit.database.DatabaseFactory.pushQuery
import org.jetbrains.exposed.sql.*

class DAOEvents: DAOTable<Event> {
    override fun resultRowToModel(row: ResultRow): Event {
        return Event(
            id = row[Events.id],
            title = row[Events.title],
            date = row[Events.date],
            time = row[Events.time],
            duration = row[Events.duration],
            description = row[Events.description],
            dateCreated =  row[Events.dateCreated]
        )
    }

    override suspend fun selectAll(): List<Event> {
        return pushQuery{
            Events.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): Event? {
        return selectMany(where).singleOrNull()
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<Event> {
        return pushQuery {
            Events.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            Events.deleteWhere(op = where) > 0
        }
    }

    override suspend fun checkExist(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return selectSingle(where) != null
    }

    override suspend fun insert(model: Event): Event? {
        return pushQuery {
            Events.insert {
                it[title] = model.title
                it[date] = model.date
                it[time] = model.time
                it[duration] = model.duration
                it[description] = model.description
                it[dateCreated] = model.dateCreated
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun edit(model: Event, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            Events.update(where){
                it[title] = model.title
                it[date] = model.date
                it[time] = model.time
                it[duration] = model.duration
                it[description] = model.description
                it[dateCreated] = model.dateCreated
            } > 0
        }
    }
}