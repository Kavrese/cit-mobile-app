package com.cit.database.tables.materials

import com.cit.database.tables.materials.TypeMaterial.Companion.toTypeMaterial
import com.cit.database.tables.users.SafeProfile
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table

data class Material(
    val id: Int,
    val title: String,
    val href: String,
    val cover: String,
    val type: TypeMaterial,
    val idAuthor: Int
){
    fun toSafe(author: SafeProfile): SafeMaterial{
        return SafeMaterial(
            id, title, href, cover, type.toString(), author
        )
    }
}

@Serializable
data class SafeMaterial(
    val id: Int,
    val title: String,
    val href: String,
    val cover: String,
    val type: String,
    val author: SafeProfile
)

enum class TypeMaterial(val title: String){
    YOUTUBE("youtube"),
    SITE("site"),
    GOOGLE_DRIVE("google drive"),
    YANDEX_DRIVE("yandex drive");

    override fun toString(): String {
        return title
    }

    companion object {
        fun String.toTypeMaterial(): TypeMaterial{
            return values().singleOrNull { it.title == this } ?: SITE
        }
    }
}

object Materials: Table(){
    val id = integer("id").autoIncrement()
    val title = varchar("title", 1000)
    val href = varchar("href", 1000)
    val cover = varchar("cover", 1000)
    val type = varchar("type", 500)
    val idAuthor = integer("id_author")

    override val primaryKey: PrimaryKey = PrimaryKey(id)
}