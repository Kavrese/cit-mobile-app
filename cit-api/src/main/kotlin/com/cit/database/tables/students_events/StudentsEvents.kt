package com.cit.database.tables.students_events

import org.jetbrains.exposed.sql.Table

data class StudentEvent(
    val id: Int,
    val idUser: Int,
    val idEvent: Int,
    val attended: Boolean
)

object StudentsEvents: Table("events_students") {
    val id = integer("id").autoIncrement()
    val idUser = integer("id_user")
    val idEvent = integer("id_event")
    val attended = bool("attended")

    override val primaryKey: PrimaryKey = PrimaryKey(id)
}