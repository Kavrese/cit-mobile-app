package com.cit.database.tables.materials_events


import com.cit.database.DAOTable
import com.cit.database.DatabaseFactory.pushQuery
import org.jetbrains.exposed.sql.*

class DAOMaterialsEvents: DAOTable<MaterialEvent> {
    override fun resultRowToModel(row: ResultRow): MaterialEvent {
        return MaterialEvent(
            id = row[MaterialsEvents.id],
            idEvent = row[MaterialsEvents.idEvent],
            idMaterial = row[MaterialsEvents.idMaterial]
        )
    }

    override suspend fun selectAll(): List<MaterialEvent> {
        return pushQuery{
            MaterialsEvents.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): MaterialEvent? {
        return selectMany(where).singleOrNull()
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<MaterialEvent> {
        return pushQuery {
            MaterialsEvents.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            MaterialsEvents.deleteWhere(op = where) > 0
        }
    }

    override suspend fun checkExist(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return selectSingle(where) != null
    }

    override suspend fun insert(model: MaterialEvent): MaterialEvent? {
        return pushQuery {
            MaterialsEvents.insert {
                it[idEvent] = model.idEvent
                it[idMaterial] = model.idMaterial
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun edit(model: MaterialEvent, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            MaterialsEvents.update(where){
                it[idEvent] = model.idEvent
                it[idMaterial] = model.idMaterial
            } > 0
        }
    }
}