package com.cit.database.tables.hometasks


import com.cit.database.DAOTable
import com.cit.database.DatabaseFactory.pushQuery
import org.jetbrains.exposed.sql.*

class DAOHomeTasks: DAOTable<HomeTask> {
    override fun resultRowToModel(row: ResultRow): HomeTask {
        return HomeTask(
            id = row[HomeTasks.id],
            title = row[HomeTasks.title],
            date = row[HomeTasks.date],
            deadline = row[HomeTasks.deadline],
            idEvent = row[HomeTasks.idEvent],
            idAuthor = row[HomeTasks.idAuthor]
        )
    }

    override suspend fun selectAll(): List<HomeTask> {
        return pushQuery{
            HomeTasks.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): HomeTask? {
        return selectMany(where).singleOrNull()
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<HomeTask> {
        return pushQuery {
            HomeTasks.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            HomeTasks.deleteWhere(op = where) > 0
        }
    }

    override suspend fun checkExist(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return selectSingle(where) != null
    }

    override suspend fun insert(model: HomeTask): HomeTask? {
        return pushQuery {
            HomeTasks.insert {
                it[title] = model.title
                it[date] = model.date
                it[deadline] = model.deadline
                it[idEvent] = model.idEvent
                it[idAuthor] = model.idAuthor
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun edit(model: HomeTask, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            HomeTasks.update(where){
                it[title] = model.title
                it[date] = model.date
                it[deadline] = model.deadline
                it[idEvent] = model.idEvent
                it[idAuthor] = model.idAuthor
            } > 0
        }
    }
}