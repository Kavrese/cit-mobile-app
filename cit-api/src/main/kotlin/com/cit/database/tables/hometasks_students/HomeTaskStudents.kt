package com.cit.database.tables.hometasks_students

import org.jetbrains.exposed.sql.Table

data class HomeTaskStudent(
    val id: Int,
    val idUser: Int,
    val idHomeTask: Int,
    val status: String
)

object HomeTaskStudents: Table("hometasks_students") {
    val id = integer("id").autoIncrement()
    val idUser = integer("id_user")
    val idHomeTask = integer("id_hometask")
    val status = varchar("status", 150)


    override val primaryKey: PrimaryKey = PrimaryKey(id)
}