package com.cit.database.tables.hometasks_requirements_students


import com.cit.database.DAOTable
import com.cit.database.DatabaseFactory.pushQuery
import com.cit.database.tables.hometasks_requirements_students.HomeTaskRequirementsStudents.id
import com.cit.database.tables.hometasks_requirements_students.HomeTaskRequirementsStudents.idHomeTask
import com.cit.database.tables.hometasks_requirements_students.HomeTaskRequirementsStudents.idUser
import com.cit.database.tables.hometasks_requirements_students.HomeTaskRequirementsStudents.requirementAnswer
import org.jetbrains.exposed.sql.*

class DAOHomeTaskRequirementStudents: DAOTable<HomeTaskRequirementStudent> {
    override fun resultRowToModel(row: ResultRow): HomeTaskRequirementStudent {
        return HomeTaskRequirementStudent(
            id = row[id],
            requirementAnswer = row[requirementAnswer],
            idHomeTask = row[idHomeTask],
            idUser = row[idUser]
        )
    }

    override suspend fun selectAll(): List<HomeTaskRequirementStudent> {
        return pushQuery{
            HomeTaskRequirementsStudents.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): HomeTaskRequirementStudent? {
        return selectMany(where).singleOrNull()
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<HomeTaskRequirementStudent> {
        return pushQuery {
            HomeTaskRequirementsStudents.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            HomeTaskRequirementsStudents.deleteWhere(op = where) > 0
        }
    }

    override suspend fun checkExist(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return selectSingle(where) != null
    }

    override suspend fun insert(model: HomeTaskRequirementStudent): HomeTaskRequirementStudent? {
        return pushQuery {
            HomeTaskRequirementsStudents.insert {
                it[requirementAnswer] = model.requirementAnswer
                it[idHomeTask] = model.idHomeTask
                it[idUser] = model.idUser
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun edit(model: HomeTaskRequirementStudent, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            HomeTaskRequirementsStudents.update(where){
                it[requirementAnswer] = model.requirementAnswer
                it[idHomeTask] = model.idHomeTask
                it[idUser] = model.idUser
            } > 0
        }
    }
}