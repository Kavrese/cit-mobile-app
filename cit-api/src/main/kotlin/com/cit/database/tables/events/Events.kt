package com.cit.database.tables.events

import com.cit.common.datePattern
import com.cit.common.timePattern
import com.cit.common.toPatternString
import com.cit.database.tables.users.SafeProfile
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.javatime.date
import org.jetbrains.exposed.sql.javatime.time
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter

data class Event(
    val id: Int,
    val title: String,
    val date: LocalDate,
    val time: LocalTime,
    val duration: String,
    val description: String,
    val dateCreated: LocalDate
){
    fun toSafeFullEvent(mentors: List<SafeProfile>,
                    studentsExist: List<SafeProfile>,
                    studentNotExist: List<SafeProfile>): SafeEventFull =
        SafeEventFull(
            id, title, date.toPatternString(datePattern),
            time.toPatternString(timePattern), duration,
            description, dateCreated.toPatternString(datePattern),
            mentors, studentsExist, studentNotExist
        )

    fun toSafeShort(): SafeEventShort =
        SafeEventShort(
            id, title, date.toPatternString(datePattern),
            time.toPatternString(timePattern), duration, description,
            dateCreated.toPatternString(datePattern)
        )
}

@kotlinx.serialization.Serializable
data class SafeEventFull(
    val id: Int,
    val title: String,
    val date: String,
    val time: String,
    val duration: String,
    val description: String,
    val dateCreated: String,
    val mentors: List<SafeProfile>,
    val studentsExist: List<SafeProfile>,
    val studentNotExist: List<SafeProfile>,
)

@kotlinx.serialization.Serializable
data class SafeEventShort(
    val id: Int,
    val title: String,
    val date: String,
    val time: String,
    val duration: String,
    val description: String,
    val dateCreated: String,
)

object Events: Table(){
    val id = integer("id").autoIncrement()
    val title = varchar("title", 5000)
    val date = date("date")
    val time = time("time")
    val duration = varchar("duration", 150)
    val description = varchar("description", 5000)
    val dateCreated = date("date_created")

    override val primaryKey: PrimaryKey = PrimaryKey(id)
}