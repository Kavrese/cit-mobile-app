package com.cit.database.tables.hometasks_requirements


import com.cit.database.DAOTable
import com.cit.database.DatabaseFactory.pushQuery
import com.cit.database.tables.hometasks_requirements.HomeTaskRequirements.id
import com.cit.database.tables.hometasks_requirements.HomeTaskRequirements.requirement
import org.jetbrains.exposed.sql.*

class DAOHomeTaskRequirements: DAOTable<HomeTaskRequirement> {
    override fun resultRowToModel(row: ResultRow): HomeTaskRequirement {
        return HomeTaskRequirement(
            id = row[id],
            requirement = row[requirement],
            idHomeTask = row[HomeTaskRequirements.idHomeTask],
        )
    }

    override suspend fun selectAll(): List<HomeTaskRequirement> {
        return pushQuery{
            HomeTaskRequirements.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): HomeTaskRequirement? {
        return selectMany(where).singleOrNull()
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<HomeTaskRequirement> {
        return pushQuery {
            HomeTaskRequirements.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            HomeTaskRequirements.deleteWhere(op = where) > 0
        }
    }

    override suspend fun checkExist(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return selectSingle(where) != null
    }

    override suspend fun insert(model: HomeTaskRequirement): HomeTaskRequirement? {
        return pushQuery {
            HomeTaskRequirements.insert {
                it[requirement] = model.requirement
                it[idHomeTask] = model.idHomeTask
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun edit(model: HomeTaskRequirement, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            HomeTaskRequirements.update(where){
                it[requirement] = model.requirement
                it[idHomeTask] = model.idHomeTask
            } > 0
        }
    }
}