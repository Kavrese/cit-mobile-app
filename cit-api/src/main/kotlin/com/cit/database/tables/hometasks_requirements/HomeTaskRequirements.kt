package com.cit.database.tables.hometasks_requirements

import org.jetbrains.exposed.sql.Table

data class HomeTaskRequirement(
    val id: Int,
    val requirement: String,
    val idHomeTask: Int,
)

object HomeTaskRequirements: Table("hometasks_requirements") {
    val id = integer("id").autoIncrement()
    val requirement = varchar("requirement", 5000)
    val idHomeTask = integer("id_hometask")


    override val primaryKey: PrimaryKey = PrimaryKey(id)
}