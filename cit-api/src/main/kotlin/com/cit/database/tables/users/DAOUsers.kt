package com.cit.database.tables.users

import com.cit.common.TypeProfile.Companion.toTypeProfile
import com.cit.database.DAOTable
import com.cit.database.DatabaseFactory.pushQuery
import org.jetbrains.exposed.sql.*

class DAOUsers: DAOTable<Profile> {
    override fun resultRowToModel(row: ResultRow): Profile = Profile(
        id = row[Users.id],
        login = row[Users.login],
        firstname = row[Users.firstname],
        lastname = row[Users.lastname],
        patronymic = row[Users.patronymic],
        password = row[Users.password],
        token = row[Users.token],
        type = row[Users.type].toTypeProfile(),
        avatar = row[Users.avatar]
    )

    override suspend fun selectAll(): List<Profile> {
        return pushQuery{
            Users.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): Profile? {
        return pushQuery{
            Users.select(where).map(::resultRowToModel).singleOrNull()
        }
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<Profile> {
        return pushQuery{
            Users.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery{
            Users.deleteWhere(op = where) > 0
        }
    }

    override suspend fun checkExist(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return selectSingle(where) != null
    }

    override suspend fun insert(model: Profile): Profile? {
        return pushQuery{
            Users.insert {
                it[login] = model.login
                it[firstname] = model.firstname
                it[lastname] = model.lastname
                it[token] = model.token
                it[patronymic] = model.patronymic
                it[password] = model.password
                it[type] = model.type.title
                it[avatar] = model.avatar
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun edit(model: Profile, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery{
            Users.update(where){
                it[login] = model.login
                it[firstname] = model.firstname
                it[lastname] = model.lastname
                it[password] = model.password
                it[token] = model.token
                it[patronymic] = model.patronymic
                it[type] = model.type.title
                it[avatar] = model.avatar
            } > 0
        }
    }

    suspend fun editToken(token: String, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean{
        return pushQuery {
            Users.update(where){
                it[Users.token] = token
            } > 0
        }
    }

}